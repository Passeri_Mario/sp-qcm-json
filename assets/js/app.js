// génération des données

const setAttributes = (element, attributes) => {
	for (const key in attributes) {
		element.setAttribute(key, attributes[key]);
	}
};

// Importation dynamique du formulaire

const createElement = (quizList) => {
	const main = document.getElementsByTagName("body")[0];
	const form = document.createElement('form');
	//form.setAttribute("method", "POST");
	quizList.forEach(element => {
		const index = quizList.indexOf(element) + 1;
		const fieldset = document.createElement('fieldset');
		fieldset.classList.add('div');
		fieldset.setAttribute("id", `Q${index}`);
		const legend = document.createElement('legend');
		const legendText = document.createTextNode(`Question ${index}`);
		fieldset.append(legend);
		legend.append(legendText);
		const question = document.createElement("p");
		const questionText = document.createTextNode(`${element.question}`);
		question.append(questionText);
		fieldset.append(question);
		const radioTrue = document.createElement('input');
		setAttributes(radioTrue, {"type": "radio", "name": `Q${index}`, "id": `${index}1`, "value": "true"});
		const labelRadioTrue = document.createElement("label");
		labelRadioTrue.setAttribute("for", `${index}1`);
		const labelTextTrue = document.createTextNode("Vrai");
		labelRadioTrue.append(labelTextTrue);
		fieldset.append(radioTrue);
		fieldset.append(labelRadioTrue);
		const radioFalse = document.createElement('input');
		setAttributes(radioFalse, {"type": "radio", "name": `Q${index}`, "id": `${index}0`, "value": "false"});
		const labelRadioFalse = document.createElement("label");
		labelRadioFalse.setAttribute("for", `${index}0`);
		const labelTextFalse = document.createTextNode("Faux");
		labelRadioFalse.append(labelTextFalse);
		fieldset.append(radioFalse);
		fieldset.append(labelRadioFalse);
		form.append(fieldset);
		main.append(form);
	});
	const submitBtn = document.createElement("button");
	setAttributes(submitBtn, {"type": "submit", "onclick": "checkAnswers(event, quizList)"});
	submitBtn.classList.add("bouton","vert");
	const submitText = document.createTextNode("Valider");
	submitBtn.append(submitText);
	form.append(submitBtn);
};

// vérification si le qcm reste bien rempli

const verifyAllChecked = (quizList) => {
	let number = 0;
	quizList.forEach(element => {
		const index = quizList.indexOf(element) + 1;
		const radio = document.getElementsByName(`Q${index}`);
		if (!radio[0].checked && !radio[1].checked) {
			radio[0].setCustomValidity("Veuillez cocher une case");
			radio[1].setCustomValidity("Veuillez cocher une case")
		} else if (radio[0].checked || radio[1].checked) {
			radio[0].setCustomValidity("")
			radio[1].setCustomValidity("")
			number++;
		}
	});

	return number === quizList.length;
}

// Traitement des réponses de l'utilisateur

const checkAnswers = (event, quizList) => {
	event.preventDefault();
	let score = 0;
	if (verifyAllChecked(quizList)) {
		quizList.forEach(element => {
			const index = quizList.indexOf(element) + 1;
			const radio = document.getElementsByName(`Q${index}`);
			const argument = document.createElement("p");
			const argumentText = document.createTextNode(element.argument);
			argument.append(argumentText);
			const fieldset = document.getElementById(`Q${index}`);
			let style;

			// Condition des réponses
			if (radio[0].checked) {
				if (radio[0].value && element.response) {
					style = "color: green";
					score++;
				} else {
					style = "color: red";
				}
			}
			else {
				if (radio[1].value && element.response) {
					style = "color: red";
				} else {
					style = "color: green";
					score++;
				}
			}
			argument.setAttribute("style", style);
			fieldset.append(argument);
			document.getElementById(`Q${index}`).disabled = true;
		});

		document.getElementsByClassName("bouton vert")[0].style.display="none";
		const results = document.createElement("div");
		results.setAttribute("id", "modal");
		const paragraph = document.createElement("p");
		const resultsText = document.createTextNode(`Votre score est de ${score} sur 8`);
		paragraph.append(resultsText);
		results.append(paragraph);
		document.getElementsByTagName("main")[0].append(results);
		window.scrollTo(0, 0);
	}
}



